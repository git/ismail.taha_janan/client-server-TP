# Clent-server TP

## Run Docker

```
docker cp /Users/etudiant/Downloads/mongodb-sample-dataset-main/sample_mflix test-mongo:/movies
```

```
mongoimport --db movies --collection collection  /movies/movies.json
```

list containers

```
docker container ls
```

run container
```
docker exec -it [ContainerID] bash
```

lancer la DB
```
mongosh
```

```
db.collection.count({genres: { $in: ["Thriller" ] }});
```

```
db.collection.find({title:{$regex : /ghost/i}});
```


```
db.collection.find({title:{$regex : /ghost/i},year: { $gt: 2013 }});
```

```
db.movies.aggregate([
   { "$sort": { "awards.wins": -1 } },
   { "$limit": 1 }
])
```

```
db.movies.aggregate([
    { "$match": { "runtime": { "$gt": 120 }, "imdb.rating": { "$lt": 2.0 }}},
    { "$sort": { "year": 1 } },
    { "$limit": 1 }
])
```


```
db.movies.aggregate([
    { "$match": { "runtime": { "$gt": 120 }, "imdb.rating": { "$lt": 2.0 } } },
    { "$sort": { "year": 1 } },
    { "$limit": 1 },
    { "$lookup": {
        "from": "comments",
        "localField": "_id",
        "foreignField": "movie_id",
        "as": "movie_comments"
    }}
])
```

```
db.movies.aggregate([
    { "$unwind": "$countries" },
    {
        "$group": {
            "_id": "$countries",
            "count": { "$sum": 1 },
            "titles": { "$push": "$title" }
        }
    },
    {
        "$project": {
            "_id": 0,
            "country": "$_id",
            "count": 1,
            "titles": 1
        }
    },
    { "$sort": { "count": -1 } }
])
```